package com.example.payday.apliksiplantae;

public class Questions {
    public String mQuestions[] = {
            "Berikut yang termasuk ke dalam golongan tumbuhan tidak berpembuluh adalah..",
                    "Fase gametofit lebih dominan dari fase sporofit terjadi pada metagenesis tumbuhan..",
                    "Salah satu anggota tumbuhan tidak berpembuluh yang disebut sebagi lumut primitif ialah..",
                    "Daunnya selalu melingkar dan menggulung pada usia muda merupkan ciri dari tumbuhan..",
                    "Organisme multiseluer memiliki klorofil dan menyimpan cadangan makanan dalam bentuk amilum merupakan ciri umum dari kingdom..",
                    "Fase sporofit bryophyta menghasilkan alat reproduksi berupa..",
                    "Organ yang menjadi ciri umum morfologi pada psilophyta adalah..",
                    "Mikrosporofil dan makrosporofil merupakan alat reproduksi seksual dari..",
                    "Kelas monokotil merupakan bagian dari sub divisi.., divisi.., Kingdom..",
                    "Berikut yang merupakan ciri umum dikotil adalah.."

    };

    private String mChoices [][] = {
            {"Psilophyta","Anthocerophyta","Cycadophyta","Ginkgophyta"},
            {"Psilophyta","Hepatophyta","Cycadophyta","Gnetophyta"},
            {"Bryophyta","Lycophyta","Psilophyta","Anthocerophyta"},
            {"Spermatophyta","Bryophyta","Hepatophyta","Pteridophyta"},
            {"Animalia","Fungi","Plantae","Eubacteria"},
            {"Anteridium","Protonema","SporaHaploid","SporaDiploid"},
            {"Sporangium","Gemma Cup","Rizhoid","Sorus"},
            {"Gnetophyta","Anthocerophyta","Anthophyta","Pteridophyta"},
            {"Ginkgophyta, angiospermae, plantae",
                    "Ginkgophyta, gymnospermae, plantae",
                    "Antophyta, gymnospermae, plantae",
                    "Antophyta, angiospermae, plantae"},
            {"Akar serabut, pembuluh tersebar, dua kotiledon",
                    "Akar tunggang, pembuluh tersebar, satu kotiledon",
                    "Akar serabut, pembuluh teratur melingkar, dua kotiledon",
                    "Akar tunggang, pembuluh teratur melingkar, dua kotiledon"}
    };

    private String mCorrectAnswers [] = {
      "Anthocerophyta",
            "Hepatophyta",
            "Anthocerophyta",
            "Pteridophyta",
            "Plantae",
            "SporaHaploid",
            "Sorus",
            "Gnetophyta",
            "Antophyta, angiospermae, plantae",
            "Akar tunggang, pembuluh teratur melingkar, dua kotiledon"
    };

    public String getQuestion(int a){
        String question = mQuestions[a];
        return question;

    }

    public String getChoice1(int a){
        String choice = mChoices[a][0];
        return choice;
    }
    public String getChoice2(int a){
        String choice = mChoices[a][1];
        return choice;
    }
    public String getChoice3(int a){
        String choice = mChoices[a][2];
        return choice;
    }
    public String getChoice4(int a){
        String choice = mChoices[a][3];
        return choice;
    }

    public  String getCorrectAnswer(int a){
        String answer = mCorrectAnswers[a];
        return answer;
    }

}