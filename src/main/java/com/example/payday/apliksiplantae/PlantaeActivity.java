package com.example.payday.apliksiplantae;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;

public class PlantaeActivity extends AppCompatActivity {
    private ImageView button;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_plantae);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle("PLANTAE");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        final MediaPlayer sound1 = MediaPlayer.create(this, R.raw.suara_mindmap);
        final MediaPlayer sound2 = MediaPlayer.create(this, R.raw.suara_kingdom);
        final MediaPlayer sound3 = MediaPlayer.create(this, R.raw.suara_peranan);
        final MediaPlayer sound4 = MediaPlayer.create(this, R.raw.suara_kamus);


        button = (ImageView) findViewById(R.id.btn__plantae_mindmap);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sound1.start();
                openMindmapActivity();
            }
        });
        button = (ImageView) findViewById(R.id.btn__plantae_kingdom);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sound2.start();
                openKingdomActivity();
            }
        });
        button = (ImageView) findViewById(R.id.btn__plantae_peran);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sound3.start();
                openPeranActivity();
            }
        });
        button = (ImageView) findViewById(R.id.btn__plantae_kamus);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sound4.start();
                openKamusActivity();
            }
        });
    }

    private void openKamusActivity() {
        Intent intent = new Intent(this, KamusActivity.class);
        startActivity(intent);
    }

    private void openPeranActivity() {
        Intent intent = new Intent(this, PeranActivity.class);
        startActivity(intent);
    }

    private void openKingdomActivity() {
        Intent intent = new Intent(this, KingdomActivity.class);
        startActivity(intent);
    }

    private void openMindmapActivity() {
        Intent intent = new Intent(this, MindmapActivity.class);
        startActivity(intent);
    }

}
