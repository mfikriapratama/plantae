package com.example.payday.apliksiplantae;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class Splashscreen extends Activity {

private ImageView imgview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splashscreen);

        final MediaPlayer kliksound = MediaPlayer.create(this, R.raw.suara_selamatdatang);
        kliksound.start();

        imgview = (ImageView) findViewById(R.id.imgview_splash);

        Animation myanim = AnimationUtils.loadAnimation(this, R.anim.transisi);
        imgview.startAnimation(myanim);

        Thread thread = new Thread() {
            public void run() {
                try {
                    sleep(5000);
                } catch (InterruptedException e){
                    e.printStackTrace();
                }
                finally {
                    startActivity(new Intent(Splashscreen.this, MainActivity.class));
                    finish();
                }
            }
        };
            thread.start();
    }
}