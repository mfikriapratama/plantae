package com.example.payday.apliksiplantae;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;

public class KuisActivity extends AppCompatActivity {
    private ImageView imgbuttonn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kuis);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final MediaPlayer soundkuis1 = MediaPlayer.create(this, R.raw.suara_kuis1);
        final MediaPlayer soundkuis2 = MediaPlayer.create(this, R.raw.suara_kuis2);
        getSupportActionBar().setTitle("KUIS");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        imgbuttonn = (ImageView) findViewById(R.id.img_btn_kuis1);
        imgbuttonn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                soundkuis1.start();
                openKuis1Activity();
            }
        });

        imgbuttonn = (ImageView) findViewById(R.id.img_btn_kuis2);
        imgbuttonn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                soundkuis2.start();
                openKuis2Activity();
            }
        });
    }

    private void openKuis2Activity() {
        Intent intent = new Intent(this, Kuis2Activity.class);
        startActivity(intent);
    }

    private void openKuis1Activity() {
        Intent intent = new Intent(this, Kuis1Activity.class);
        startActivity(intent);
    }

}
