package com.example.payday.apliksiplantae;

import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {
    private ImageView imgbuttonm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final MediaPlayer kliksound1 = MediaPlayer.create(this, R.raw.suara_referensi);
        final MediaPlayer kliksound2 = MediaPlayer.create(this, R.raw.suara_silabus);
        final MediaPlayer kliksound3 = MediaPlayer.create(this, R.raw.suara_kuis);
        final MediaPlayer kliksound4 = MediaPlayer.create(this, R.raw.suara_materi);

        imgbuttonm = findViewById(R.id.img_btn_ref);
        imgbuttonm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                kliksound1.start();
                openReferensiActivity();
            }
        });

        imgbuttonm = findViewById(R.id.img_btn_sil);
        imgbuttonm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                kliksound2.start();
                openSilabusActivity();
            }
        });

        imgbuttonm = findViewById(R.id.img_btn_kuis);
        imgbuttonm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                kliksound3.start();
                openKuisActivity();
            }
        });

        imgbuttonm = findViewById(R.id.img_btn_materi);
        imgbuttonm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                kliksound4.start();
                openPlantaeActivity();
            }
        });

    }

    private void openPlantaeActivity() {
        Intent intent = new Intent(this, PlantaeActivity.class);
        startActivity(intent);
    }

    private void openKuisActivity() {
        Intent intent = new Intent(this, KuisActivity.class);
        startActivity(intent);
    }

    private void openSilabusActivity() {
        Intent intent = new Intent(this, SilabusActivity.class);
        startActivity(intent);
    }

    private void openReferensiActivity() {
        Intent intent = new Intent(this, ReferensiActivity.class);
        startActivity(intent);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        final MediaPlayer kliksound5 = MediaPlayer.create(this, R.raw.suara_profil);
        final MediaPlayer kliksound6 = MediaPlayer.create(this, R.raw.suara_ttgapk);
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_profil) {

            Intent myintent = new Intent(MainActivity.this,
                    ProfilActivity.class);
            kliksound5.start();
            startActivity(myintent);
            return false;
        } else if (id == R.id.action_ttg_apk) {

            Intent myintent = new Intent(MainActivity.this,
                    TentangApkActivity.class);
            kliksound6.start();
            startActivity(myintent);
            return false;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        //exit dengan konfirmasi alertDialog
        final MediaPlayer kliksound = MediaPlayer.create(this, R.raw.sound_exitapp);
        kliksound.start();
        final AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setMessage("Keluar dari Aplikasi PLantae?");
        builder.setCancelable(true);
        builder.setNegativeButton("TIDAK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });
        builder.setPositiveButton("YA", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                finish();
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    public void ClickExit(View view) {
        onBackPressed();
    }
}