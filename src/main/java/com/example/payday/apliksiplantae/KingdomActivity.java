package com.example.payday.apliksiplantae;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;

public class KingdomActivity extends AppCompatActivity {
ImageView imageview;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kingdom);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final MediaPlayer suara1 = MediaPlayer.create(this, R.raw.suara_ttb);
        final MediaPlayer suara2 = MediaPlayer.create(this, R.raw.suara_tb);

        getSupportActionBar().setTitle("KINGDOM PLANTAE");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        imageview = findViewById(R.id.imgview_ttb);
        imageview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                suara1.start();
                startActivity(new Intent(KingdomActivity.this, KingdomTTBActivity.class));
            }
        });

        imageview = findViewById(R.id.imgview_tb);
        imageview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                suara2.start();
                startActivity(new Intent(KingdomActivity.this, KingdomTBActivity.class));
            }
        });
    }

}
