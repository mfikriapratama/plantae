package com.example.payday.apliksiplantae;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;

public class Kuis2Activity extends AppCompatActivity {

    TextView tv_question;
    Button b_true, b_false;

    Questions2 mQuestions2;
    int questionsLength2;

    ArrayList<Item> questionsList2;
    int currentQuestion2 = 0 ;
    boolean winner = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kuis2);


        //init stuff
        tv_question = (TextView) findViewById(R.id.tv_question);
        b_true = (Button) findViewById(R.id.b_true);
        b_false = (Button) findViewById(R.id.b_false);

        mQuestions2 = new Questions2();
        questionsLength2 = mQuestions2.mQuestions2.length;

        questionsList2 = new ArrayList<>();

        //save all the questions in the list

        for (int i = 0; i < questionsLength2; i++) {
            questionsList2.add(new Item(mQuestions2.getQuestion2(i), mQuestions2.getAnswer2(i)));
        }

        //shuffle the question
        Collections.shuffle(questionsList2);

        //start the game
        setQuestions2(currentQuestion2);

        b_true.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkQuestion(currentQuestion2)){
                    //correct - the game continues
                    currentQuestion2++;
                    if (currentQuestion2 < questionsLength2) {
                        setQuestions2(currentQuestion2);
                    } else {
                        //game over - winner
                        winner = true;
                        endGame();
                    }
                }else {
                    //wrong - the game ends
                    endGame();
                }
            }
        });

        b_false.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!checkQuestion(currentQuestion2)){
                    //correct - the game continues
                    currentQuestion2++;
                    if (currentQuestion2 < questionsLength2) {
                        setQuestions2(currentQuestion2);
                    } else {
                        //game over - winner
                        winner = true;
                        endGame();
                    }
                }else {
                    //wrong - the game ends
                    endGame();
                }
            }
        });

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    //show question on the screen
    private void setQuestions2(int number) {
        tv_question.setText(questionsList2.get(number).getQuestion());
    }

    //check if te answer is right
    private boolean checkQuestion(int number){
        String answer2 = questionsList2.get(number).getAnswer();
        return answer2.equals("true");
    }

    //game over
    private void endGame(){
        if(winner){
            Toast.makeText(this, "Kamu Hebat!!!", Toast.LENGTH_SHORT).show();
            finish();
        } else {
            Toast.makeText(this, "Ups.. Jawaban Salah, Belajar Lagi Ya!!! ", Toast.LENGTH_SHORT).show();
            finish();
        }
    }
}