package com.example.payday.apliksiplantae;

public class Questions2 {
    public  String mQuestions2[] = {
            "Kingdom Plantae merupkan organisme multiseluler atau terdiri dari banyak sel.",
            "Bryophyta merupakan salah satu jenis tumbuhan tak berpembuluh.",
            "Struktur mirip akar pada bryophyta disebut dengan 'stipe'.",
            "Protalium tergolong ke dalam generasi pteridophyta yang memproduksi spora.",
            "Persamaan yang ada diantara tumbuhan paku dan lumut terletak pada struktur gametofitnya.",
            "Getah pada Musa paradisiaca dapat dijadikan obat luka.",
            "Gymonspermae merupakan tumbuhan berbiji tertutup yang memiliki bunga sesungguhnya.",
            "Monokotil dan dikotil merupkan anggota dari divisi Antophyta.",
            "Tropofil dan sporofil merupakan dua jenis daun yang dimiliki oleh pteridophyta.",
            "Bryophyta tidak memiliki fase pergiliran keturunan atau metagenensis."
    };

    public String mAnswers2[] = {
            "true",
            "true",
            "false",
            "false",
            "false",
            "true",
            "false",
            "true",
            "true",
            "false"
    };

    public String getQuestion2(int number){
        return mQuestions2[number];
    }
    public String getAnswer2(int number){
        return mAnswers2[number];
    }
}