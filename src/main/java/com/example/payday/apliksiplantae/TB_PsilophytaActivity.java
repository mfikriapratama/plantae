package com.example.payday.apliksiplantae;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;

public class TB_PsilophytaActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tb_psilophyta);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        final MediaPlayer psilophytaasound = MediaPlayer.create(this, R.raw.psilophytaa);
        Button playHepatophyta = (Button) this.findViewById(R.id.btn_suara_psilophyta);

        playHepatophyta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                psilophytaasound.start();
            }
        });
    }

}
