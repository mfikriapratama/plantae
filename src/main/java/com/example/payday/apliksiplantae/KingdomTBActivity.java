package com.example.payday.apliksiplantae;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;

public class KingdomTBActivity extends AppCompatActivity {
ImageView imgview;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kingdom_tb);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle("TUMBUHAN BERPEMBULUH");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        final MediaPlayer psilophytaasound = MediaPlayer.create(this, R.raw.psilophytaa);
        final MediaPlayer lycophytasound = MediaPlayer.create(this, R.raw.suara_lycophyta);
        final MediaPlayer equisetophytasound = MediaPlayer.create(this, R.raw.suara_equisetophyta);
        final MediaPlayer pterophytasound = MediaPlayer.create(this, R.raw.suara_pterophyta);
        final MediaPlayer pinophytasound = MediaPlayer.create(this, R.raw.suara_pinophyta);
        final MediaPlayer cycadophytasound = MediaPlayer.create(this, R.raw.suara_cycadophyta);
        final MediaPlayer ginkgophytasound = MediaPlayer.create(this, R.raw.suara_ginkgophyta);
        final MediaPlayer gnetophytasound = MediaPlayer.create(this, R.raw.suara_gnetophyta);
        final MediaPlayer anthophytasound = MediaPlayer.create(this, R.raw.suara_anthophyta);

        imgview = (ImageView) findViewById(R.id.imgview_psylophyta);
        imgview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                psilophytaasound.start();
                openTB_PsilophytaActivity();
            }
        });
        imgview = (ImageView) findViewById(R.id.imgview_lycophyta);
        imgview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lycophytasound.start();
                openTB_LycophytaActivity();
            }
        });
        imgview = (ImageView) findViewById(R.id.imgview_equisetophyta);
        imgview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                equisetophytasound.start();
                openTB_EquisetophytaActivity();
            }
        });
        imgview = (ImageView) findViewById(R.id.imgview_pterophyta);
        imgview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pterophytasound.start();
                openPterophytaActivity();
            }
        });
        imgview = (ImageView) findViewById(R.id.imgview_pinophyta);
        imgview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pinophytasound.start();
                openTB_PinophytaActivity();
            }
        });
        imgview = (ImageView) findViewById(R.id.imgview_cycadophyta);
        imgview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cycadophytasound.start();
                openCycadophytaActivity();
            }
        });
        imgview = (ImageView) findViewById(R.id.imgview_ginkgophyta);
        imgview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ginkgophytasound.start();
                openTB_GinkgophytaActivity();
            }
        });
        imgview = (ImageView) findViewById(R.id.imgview_gnetophyta);
        imgview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gnetophytasound.start();
                openGnetophytaActivity();
            }
        });
        imgview = (ImageView) findViewById(R.id.imgview_anthophyta);
        imgview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                anthophytasound.start();
                openTB_AnthophytaActivity();
            }
        });
    }

    private void openTB_AnthophytaActivity() {
        Intent intent = new Intent(this, TB_Anthophyta.class);
        startActivity(intent);
    }

    private void openGnetophytaActivity() {
        Intent intent = new Intent(this, Gnetophyta.class);
        startActivity(intent);
    }

    private void openTB_GinkgophytaActivity() {
        Intent intent = new Intent(this, TB_Ginkgophyta.class);
        startActivity(intent);
    }

    private void openCycadophytaActivity() {
        Intent intent = new Intent(this, Cycadophyta.class);
        startActivity(intent);
    }

    private void openTB_PinophytaActivity() {
        Intent intent = new Intent(this, TB_Pinophyta.class);
        startActivity(intent);
    }

    private void openPterophytaActivity() {
        Intent intent = new Intent(this, Pterophyta.class);
        startActivity(intent);
    }

    private void openTB_EquisetophytaActivity() {
        Intent intent = new Intent(this, TB_Equisetophyta.class);
        startActivity(intent);
    }

    private void openTB_LycophytaActivity() {
        Intent intent = new Intent(this, TB_Lycophyta.class);
        startActivity(intent);
    }

    private void openTB_PsilophytaActivity() {
        Intent intent = new Intent(this, TB_PsilophytaActivity.class);
        startActivity(intent);
    }

}
