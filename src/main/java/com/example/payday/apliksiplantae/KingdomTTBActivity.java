package com.example.payday.apliksiplantae;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;

public class KingdomTTBActivity extends AppCompatActivity {
ImageView imgview;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kingdom_ttb);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle("TUMBUHAN TIDAK BERPEMBULUH");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        final MediaPlayer hepatosound = MediaPlayer.create(this, R.raw.hepatopita);
        final MediaPlayer bryosound = MediaPlayer.create(this, R.raw.suara_bryophyta);
        final MediaPlayer anthocerosound = MediaPlayer.create(this, R.raw.suara_anthocerophyta);

        imgview = (ImageView) findViewById(R.id.imgview_heptophyta);
        imgview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hepatosound.start();
                openTTB_Hepatophyta();
            }
        });
        imgview = (ImageView) findViewById(R.id.imgview_bryophyta);
        imgview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bryosound.start();
                openTTB_Bryophyta();
            }
        });
        imgview = (ImageView) findViewById(R.id.imgview_anthocerophyta);
        imgview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                anthocerosound.start();
                openTTB_Anthocerophyta();
            }
        });
    }

    private void openTTB_Anthocerophyta() {
        Intent intent = new Intent(this, TTB_Anthocerophyta.class);
        startActivity(intent);
    }

    private void openTTB_Bryophyta() {
        Intent intent = new Intent(this, TTB_Bryophyta.class);
        startActivity(intent);
    }

    private void openTTB_Hepatophyta() {
        Intent intent = new Intent(this, TTB_Hepatophyta.class);
        startActivity(intent);
    }

}
