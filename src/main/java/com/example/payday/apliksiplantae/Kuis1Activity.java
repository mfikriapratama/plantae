package com.example.payday.apliksiplantae;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.Random;

public class Kuis1Activity extends AppCompatActivity {

    Button answer1,answer2, answer3, answer4;
    TextView scoree, questionn;

    private Questions mQuestions = new Questions();
    private String mAnswer;
    private int mScore = 0 ;
    private  int mQuestionsLenght = mQuestions.mQuestions.length;

    Random r;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kuis1);

        r = new Random();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        answer1 = (Button) findViewById(R.id.answer1);
        answer2 = (Button) findViewById(R.id.answer2);
        answer3 = (Button) findViewById(R.id.answer3);
        answer4 = (Button) findViewById(R.id.answer4);

        scoree = (TextView) findViewById(R.id.score);
        questionn = (TextView) findViewById(R.id.question);
        scoree.setText("SKOR : " + mScore );
        updateQuestion(r.nextInt(mQuestionsLenght));

        answer1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (answer1.getText() == mAnswer) {
                    mScore++;
                    scoree.setText("SKOR : " + mScore );
                    updateQuestion(r.nextInt(mQuestionsLenght));
                } else {
                    gameover();
                }
            }
        });

        answer2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (answer2.getText() == mAnswer) {
                    mScore++;
                    scoree.setText("SKOR : " + mScore);
                    updateQuestion(r.nextInt(mQuestionsLenght));
                } else {
                    gameover();
                }
            }
        });

        answer3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (answer3.getText() == mAnswer) {
                    mScore++;
                    scoree.setText("SKOR : " + mScore);
                    updateQuestion(r.nextInt(mQuestionsLenght));
                } else {
                    gameover();
                }
            }
        });

        answer4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (answer4.getText() == mAnswer) {
                    mScore++;
                    scoree.setText("SKOR : " + mScore);
                    updateQuestion(r.nextInt(mQuestionsLenght));
                } else {
                    gameover();
                }
            }
        });
    }

    private void gameover() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(Kuis1Activity.this);
        alertDialogBuilder
                .setMessage("Belajar Lagi!, Skor Kamu : " + mScore + " Poin")
                .setCancelable(false)
                .setPositiveButton("Mulai Lagi ?",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                startActivity(new Intent(getApplicationContext(), Kuis1Activity.class));
                            }
                        })
                .setNegativeButton("Keluar",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                finish();
                            }
                        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();

    }

    private void updateQuestion(int num) {
        questionn.setText(mQuestions.getQuestion(num));
        answer1.setText(mQuestions.getChoice1(num));
        answer2.setText(mQuestions.getChoice2(num));
        answer3.setText(mQuestions.getChoice3(num));
        answer4.setText(mQuestions.getChoice4(num));

        mAnswer = mQuestions.getCorrectAnswer(num);
    }
}