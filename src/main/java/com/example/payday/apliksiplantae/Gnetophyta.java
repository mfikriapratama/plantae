package com.example.payday.apliksiplantae;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;

public class Gnetophyta extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gnetophyta);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        final MediaPlayer kliksound = MediaPlayer.create(this, R.raw.suara_gnetophyta);
        Button playHepatophyta = (Button) this.findViewById(R.id.btn_suara_gnetophyta);

        playHepatophyta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                kliksound.start();
            }
        });

    }

}
