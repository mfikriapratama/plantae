package com.example.payday.apliksiplantae;

import android.app.SearchManager;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.SearchView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class KamusActivity extends AppCompatActivity {

    private ExpandableListView listView;
    private ExpandableListAdapter listAdapter;
    private List<String> listDataHeader;
    private HashMap<String, List<String>> listHash;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kamus);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle("KAMUS PLANTAE");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        listView = (ExpandableListView) findViewById(R.id.lvExp);
        initData();
        listAdapter = new ExpandableListAdapter(this, listDataHeader,listHash);
        listView.setAdapter(listAdapter);
    }

    private void initData() {
        listDataHeader = new ArrayList<>();
        listHash = new HashMap<>();

        listDataHeader.add("Angiospermae");
        listDataHeader.add("Anteridium");
        listDataHeader.add("Arkegonium");
        listDataHeader.add("Dikotil");
        listDataHeader.add("Dioecious");
        listDataHeader.add("Fotosintesis");
        listDataHeader.add("Gametofit");
        listDataHeader.add("Gymnospermae");
        listDataHeader.add("Lumut");
        listDataHeader.add("Monoecious");
        listDataHeader.add("Monokotil");
        listDataHeader.add("Paku heterospora");
        listDataHeader.add("Paku homospora");
        listDataHeader.add("Paku peralihan");
        listDataHeader.add("Petal");
        listDataHeader.add("Pistilum");

        List<String> Angiospermae = new ArrayList<>();
        Angiospermae.add("Tumbuhan berbiji tertutup");
        List<String> Anteridium = new ArrayList<>();
        Anteridium.add("Tempat perkembangan gamet jantan");
        List<String> Arkegonium = new ArrayList<>();
        Arkegonium.add("Tempat perkembangan gamet betina");
        List<String> Dikotil = new ArrayList<>();
        Dikotil.add("Tumbuhan biji berkeping dua");
        List<String> Dioecious = new ArrayList<>();
        Dioecious.add("Keadaan dimana arkegonia dan anteridia berada pada individu yang berbeda");
        List<String> Fotosintesis = new ArrayList<>();
        Fotosintesis.add("Proses pengubahan karbon dioksida dan air melalui bantuan matahari untuk membentuk senyawa karbohidrat");
        List<String> Gametofit = new ArrayList<>();
        Gametofit.add("Bentuk haploid multiseluler dalam organisme yang sedang mengalami pergantian generasi");
        List<String> Gymnospermae = new ArrayList<>();
        Gymnospermae.add("Tumbuhan berbiji terbuka");
        List<String> Lumut = new ArrayList<>();
        Lumut.add("Tumbuhan yang tidak memiliki pembuluh");
        List<String> Monoecious = new ArrayList<>();
        Monoecious.add("Keadaan dimana arkegonia dan anteridia berada di dalam tumbuhan yang sama");
        List<String> Monokotil = new ArrayList<>();
        Monokotil.add("Tumbuhan biji berkeping satu");
        List<String> Pakuheterospora = new ArrayList<>();
        Pakuheterospora.add("Menghasilkan spora dengan jenis dan ukuran berbeda, contohnya");
        List<String> Pakuhomospora = new ArrayList<>();
        Pakuhomospora.add("Menghasilkan spora dengan jenis dan ukuran yang sama");
        List<String> Pakuperalihan = new ArrayList<>();
        Pakuperalihan.add("Menghasilkan spora dengan bentuk dan ukuran sama namun berjenis kelamin jantan atau betina");
        List<String> Petal = new ArrayList<>();
        Petal.add("Helai mahkota bunga");
        List<String> Pistilum = new ArrayList<>();
        Pistilum.add("Organ reproduktif betina pada bunga");



        listHash.put(listDataHeader.get(0),Angiospermae);
        listHash.put(listDataHeader.get(1),Anteridium);
        listHash.put(listDataHeader.get(2),Arkegonium);
        listHash.put(listDataHeader.get(3),Dikotil);
        listHash.put(listDataHeader.get(4),Dioecious);
        listHash.put(listDataHeader.get(5),Fotosintesis);
        listHash.put(listDataHeader.get(6),Gametofit);
        listHash.put(listDataHeader.get(7),Gymnospermae);
        listHash.put(listDataHeader.get(8),Lumut);
        listHash.put(listDataHeader.get(9),Monoecious);
        listHash.put(listDataHeader.get(10),Monokotil);
        listHash.put(listDataHeader.get(11),Pakuheterospora);
        listHash.put(listDataHeader.get(12),Pakuhomospora);
        listHash.put(listDataHeader.get(13),Pakuperalihan);
        listHash.put(listDataHeader.get(14),Petal);
        listHash.put(listDataHeader.get(15),Pistilum);
    }

}