package com.example.payday.apliksiplantae;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.ImageButton;
import android.widget.ImageView;

import java.util.Objects;

public class ProfilActivity extends AppCompatActivity {
    private ImageView imgbutton, imgbutton2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profil);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        final MediaPlayer soundprofdev = MediaPlayer.create(this, R.raw.suara_profildev);
        final MediaPlayer soundprofsek = MediaPlayer.create(this, R.raw.suara_profilsek);

        Objects.requireNonNull(getSupportActionBar()).setTitle("PROFIL");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        imgbutton = findViewById(R.id.img_btn_profildev);
        imgbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                soundprofdev.start();
                openProfilDevActivity();
            }
        });

        imgbutton2 = findViewById(R.id.img_btn_profilsklh);
        imgbutton2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                soundprofsek.start();
                openProfilSekActivity();
            }
        });
    }

    private void openProfilSekActivity() {
        Intent intent = new Intent(this, ProfilSekActivity.class);
        startActivity(intent);

    }

    private void openProfilDevActivity() {
        Intent intent = new Intent(this, ProfilDevActivity.class);
        startActivity(intent);
    }
}